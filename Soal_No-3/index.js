var express = require('express');
var app = express();

const arrayOfObject = [
    {
        nama: "Smith",
        hobi: "Self Service"
    },
    {
        nama: "Dio",
        hobi: "Design Grafis"
    },
    {
        nama: "Agung",
        hobi: "Bermain Game"
    }
];

var result = [{
    nama: arrayOfObject[2].nama,
    hobi: arrayOfObject[0].hobi
}];

app.get('/', function (req, res) {
  res.status(200).json({
      result: result
  });
});

app.listen(3000)