var express = require('express');
var app = express();

const produk = [
    {
        id_produk: 1,
        nama_produk: 'Huawei P30 Pro'
    },
    {
        id_produk: 2,
        nama_produk: 'Huawei Nova 5T'
    }
];

const stok_produk = [
    {
        id_produk: 1,
        qty: 15
    },
    {
        id_produk: 1,
        qty: 6
    },
    {
        id_produk: 2,
        qty: 4
    },
    {
        id_produk: 2,
        qty: 18
    }
];

var array = [];

var object = {};

for(let i = 0; i < produk.length; i++) {
    var total = 0;
    stok_produk.forEach(stok => { 
        if(produk[i].id_produk == stok.id_produk) {
            total += stok.qty;
        };
    });
    object = {
        nama_produk: produk[i].nama_produk,
        total_stock: total
    };
    array.push(object)
}

app.get('/', function (req, res) {
  res.status(200).json({
      hasil: array
  });
});

app.listen(3000)