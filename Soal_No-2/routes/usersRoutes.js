const express = require('express');
const router = express.Router();
const usersValidator = require('../middlewares/validators/usersValidator.js');
const UsersController = require('../controllers/usersController.js');

router.post('/create', usersValidator.create, UsersController.create);

router.get('/', UsersController.getAll);

router.put('/update/:usersId', usersValidator.update, UsersController.update);

router.delete('/delete/:usersId', usersValidator.delete, UsersController.delete);

module.exports = router;