const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const usersRoutes = require('./routes/usersRoutes.js');

app.use(cors());

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({
    extended: true
}));

app.use('/users', usersRoutes);

app.listen(3000, () => console.log(`Server running on http://localhost:3000`));