const { users } = require('../models');

class UsersController {
    async create(req, res) {
        try {
            const createUsers = await users.create({
                nama: req.body.nama,
                hobi: req.body.hobi,
                alamat: req.body.alamat,
                nomor_telp: req.body.nomor_telp
            });

            const findNewUsers = await users.findOne({
                _id: createUsers._id
            }, '_id nama hobi alamat nomor_telp createdAt');

            return res.status(200).json({
                message: `Success create users data!`,
                data: findNewUsers
            });

        } catch (e) {
            console.error(e);

            return res.status(500).json({
                message: `Something went wrong!`
            });
        }
    };

    async getAll(req, res) {
        try {
            const findUsers = await users.find({}, '_id nama hobi alamat nomor_telp createdAt');

            return res.status(200).json({
                message: `Success get all users data!`,
                data: findUsers
            });

        } catch (e) {
            console.error(e);

            return res.status(500).json({
                message: `Something went wrong!`
            });
        };
    };

    async update(req, res) {
        try {
            const updateUsers = await users.findOneAndUpdate({
                _id: req.params.usersId
            }, {
                nama: req.body.nama,
                hobi: req.body.hobi,
                alamat: req.body.alamat,
                nomor_telp: req.body.nomor_telp
            }, {
                new: true
            });

            return res.status(200).json({
                message: `Success update users data!`,
                data: updateUsers
            });

        } catch (e) {
            console.error(e);

            return res.status(500).json({
                message: `Something went wrong!`
            });
        };
    };

    async delete(req, res) {
        try {
            const deleteUsers = await users.delete({
                _id: req.params.usersId
            });

            return res.status(200).json({
                message: `Delete users with ID ${req.params.usersId} success!`
            });
            
        } catch (e) {
            console.error(e);

            return res.status(500).json({
                message: `Something went wrong!`
            });
        };
    };
};

module.exports = new UsersController;