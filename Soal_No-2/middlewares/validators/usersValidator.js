const { check, sanitize, matchedData, validationResult } = require('express-validator');
const { users, expiredToken } = require('../../models');
var ObjectId = require('mongoose').Types.ObjectId; 

function isEmptyOrSpaces(str) {
    return str === null || str.match(/^ *$/) !== null;
};

function returnHexaNumber(s) {
    var regExp = /^[-+]?[0-9A-Fa-f]+\.?[0-9A-Fa-f]*?$/;
    return (typeof s === 'string' && regExp.test(s));
};

module.exports = {
    create: [
        check('nama', 'Nama must be a string!').isString().custom(async (value) => {
            if(isEmptyOrSpaces(value)) {
                throw new Error(`Nama must not be empty and not spaces!`)
            };
        }),
        check('hobi', 'Hobi must be a string!').isString().custom(async (value) => {
            if(isEmptyOrSpaces(value)) {
                throw new Error(`Hobi must not be empty and not spaces!`)
            };
        }),
        check('alamat', 'Alamat must be a string!').isString().custom(async (value) => {
            if(isEmptyOrSpaces(value)) {
                throw new Error(`Alamat must not be empty and not spaces!`)
            };
        }),
        check('nomor_telp', 'Nomor telpon must be a number and not empty!').notEmpty().isNumeric(),
        check('nomor_telp', 'Nomor telp must contains 7-14 characters!').isLength({
            min: 7,
            max: 14
        }),
        check('nomor_telp').custom(async (value) => {
            const index0 = value.split('')[0];

            console.log(index0)

            if(index0 == 0) {
                throw new Error('Nomor telepon must be started with country code! (For example 62811553322)');
            };
        }),
        (req, res, next) => {
            const errors = validationResult(req);

            if(!errors.isEmpty()) {
                return res.status(422).json({
                    message: `Error!`,
                    errors: errors.mapped()
                });
            };

            next();
        }
    ],

    update: [
        check('usersId').custom(async (value) => {
            if(value.length != 24 || !returnHexaNumber(value)) {
                throw new Error(`Users ID should have 24 characters and hexa decimal number!`);
            };

            const findUsers = await users.findOne({
                _id: value
            });

            if(!findUsers) {
                throw new Error(`Users ID not found!`)
            };
        }),
        check('nama', 'Nama must be a string!').isString().custom(async (value) => {
            if(isEmptyOrSpaces(value)) {
                throw new Error(`Nama must not be empty and not spaces!`)
            };
        }),
        check('hobi', 'Hobi must be a string!').isString().custom(async (value) => {
            if(isEmptyOrSpaces(value)) {
                throw new Error(`Hobi must not be empty and not spaces!`)
            };
        }),
        check('alamat', 'Alamat must be a string!').isString().custom(async (value) => {
            if(isEmptyOrSpaces(value)) {
                throw new Error(`Alamat must not be empty and not spaces!`)
            };
        }),
        check('nomor_telp', 'Nomor telpon must be a number and not empty!').notEmpty().isNumeric(),
        check('nomor_telp', 'Nomor telp must contains 7-14 characters!').isLength({
            min: 7,
            max: 14
        }),
        check('nomor_telp').custom(async (value) => {
            const index0 = value.split('')[0];

            console.log(index0)

            if(index0 == 0) {
                throw new Error('Nomor telepon must be started with country code! (For example 62811553322)');
            };
        }),
        (req, res, next) => {
            const errors = validationResult(req);

            if(!errors.isEmpty()) {
                return res.status(422).json({
                    message: `Error!`,
                    errors: errors.mapped()
                });
            };

            next();
        }
    ],

    delete: [
        check('usersId').custom(async (value) => {
            if(value.length != 24 || !returnHexaNumber(value)) {
                throw new Error(`Users ID should have 24 characters and hexa decimal number!`);
            };

            const findUsers = await users.findOne({
                _id: value
            });

            if(!findUsers) {
                throw new Error(`Users ID not found!`)
            };
        }),
        (req, res, next) => {
            const errors = validationResult(req);

            if(!errors.isEmpty()) {
                return res.status(422).json({
                    message: `Error!`,
                    errors: errors.mapped()
                });
            };

            next();
        }
    ]
}