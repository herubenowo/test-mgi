const path = require('path');
require('dotenv').config({
    path: `./environments/.env.${process.env.NODE_ENV}`
})
const mongoose = require('mongoose');

const uri = process.env.MONGO_URI

mongoose.connect(uri, {
    // handle all deprecation warning
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useFindAndModify: false
});

const users = require('./users.js');

module.exports = { users };