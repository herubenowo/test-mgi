const mongoose = require('mongoose');
const mongoose_delete = require('mongoose-delete');

const UsersSchema = new mongoose.Schema({
    nama: {
        type: String,
        required: true
    },
    hobi: {
        type: String,
        required: true
    },
    alamat: {
        type: String,
        required: true
    },
    nomor_telp: {
        type: Number,
        required: true
    },
}, {
    timestamps: {
        createdAt: 'createdAt',
        updatedAt: 'updatedAt'
    },
    versionKey: false,
    // activate getters
    toJSON: {
        getters: true
    }
});

UsersSchema.plugin(mongoose_delete, {
    // will exclude all deleted document from result
    overrideMethods: 'all',
    deletedAt: true
});

module.exports = comment = mongoose.model('users', UsersSchema, 'users')